const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Product = require('../models/product');

router.get('/', (req, res, next) => {
    Product.find()
        .then(data => {
            res.status(200).json({ data });
        })
        .catch(error => {
            res.status(500).json({ error });
        });
});

router.post('/', (req, res, next) => {
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price
    });
    
    product.save()
        .then(data => {
            res.status(200).json({ data });
        })
        .catch(error => {
            res.status(500).json({ error });
        });
});

router.get('/:productId', (req, res, next) => {
    const id = req.params.productId;

    Product.findById(id)
        .then(data => {
            if (data) {
                res.status(200).json({ data });
            } else {
                res.status(404).json({ error: 'No data found' });   
            }
        })
        .catch(error => {
            res.status(500).json({ error });
        });
});

router.patch('/:productId', (req, res, next) => {
    const id = req.params.productId;
    const updateOps = {};

    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
        console.log(ops); 
    }
    
    Product.update({_id: id}, { $set: updateOps})
        .then(data => {
            if (data.n) {
                res.status(200).json({ data });
            } else {
                res.status(404).json({ error: 'No data found' });   
            }
        })
        .catch(error => {
            res.status(500).json({ error });
        });
});

router.delete('/:productId', (req, res, next) => {
    const id = req.params.productId;

    Product.remove({_id: id})
        .then(data => {
            if (data.n) {
                res.status(200).json({ data });
            } else {
                res.status(404).json({ error: 'No data found' });   
            }
        })
        .catch(error => {
            res.status(500).json({ error });
        });
});

module.exports = router;