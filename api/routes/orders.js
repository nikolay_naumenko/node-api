const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.status(200).json({
        message: 'Orders were fetched'
    });  
});

router.post('/', (req, res, next) => {
    const order = {
        productId: req.body.productId,
        quantity: req.body.quantity
    };
    res.status(200).json({
        message: 'Order was created',
        createdOrder: order
    });  
});

router.get('/:orderId', (req, res, next) => {
    const id = req.params.orderId;
    
    res.status(200).json({
        message: 'GET data about the order with ID: ' + id
    });
});

router.patch('/:orderId', (req, res, next) => {
    const id = req.params.orderId;
    
    res.status(200).json({
        message: 'PATCH data of the order ID: ' + id
    }); 
});

router.delete('/:orderId', (req, res, next) => {
    const id = req.params.orderId;
    
    res.status(200).json({
        message: 'DELETE order with ID: ' + id
    }); 
});

module.exports = router;